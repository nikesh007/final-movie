import React, { Component } from 'react';
import './searchForm.scss'
import { connect } from 'react-redux';
import { searchMovie, fetchMovies, setLoading } from '../../../actions/searchActions';

class SearchForm extends Component {

  onChange = e => {
    this.props.searchMovie(e.target.value);
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.fetchMovies(this.props.text);
    this.props.setLoading();
  };

  render() {
    return (
      <div className="topnav">
        <div className="container">
          <h1>
            Search movie..
          </h1>
          <form className="search" onSubmit={this.onSubmit}>
            <input
              type="text"
              className="form-control"
              name="searchText"
              placeholder="Search Movies"
              onChange={this.onChange}
            />
            <button type="submit" className="searchButton">
              Search
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  text: state.movies.text
});

export default connect(
  mapStateToProps,
  { searchMovie, fetchMovies, setLoading }
)(SearchForm);
