import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './../reducers/searchReducer';

export const teststore = (initialstate)=>{
 const createstorewithmiddleware =    applyMiddleware(thunk)(createStore);
 return createstorewithmiddleware(rootReducer,initialstate);
}