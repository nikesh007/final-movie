import moxios from 'moxios';
import {fetchMovies} from './searchActions';
import {teststore} from './../utils';

describe('searchmovie',()=>{
    beforeEach(()=>{
        moxios.install();
    })
    
    afterEach(()=>{
        moxios.uninstall();
    })
    
    test('store is updated correctly',()=>{
        const expstate ={
            text: 'horror',
            movies: ['horrora','horrorb'],
            loading: false,
            movie: []
          };
    
         moxios.wait(()=>{
             const request = moxios.requests.mostRecent();
             request.respondWith({
                 status:200,
                 response:expstate
             })
         });
         const store = teststore();
         return store.dispatch(fetchMovies())
    })
});